#!/bin/bash

if [ "$EUID" -eq 0 ]; then
  echo "Please run as a normal user!"
  echo "This script is NOT intended to run as root."
  exit
fi

sleep_time=5
echo "----------------------------------------------"
echo "Please setup a new chroot using dev-env-setup" 
echo "./mk-deb-chroot litex"
echo "./cp-scripts-to-chroot litex"
echo "Enter the new chroot"
echo "schroot -c litex"
echo "Run within 'sudo bash'"
echo "./install-hdl-apt-req"
echo "The script will wait for $sleep_time second/s before starting"
echo "Press Ctrl-C now to cancel."
echo "----------------------------------------------"
sleep $sleep_time

sudo apt install -y vim
sudo pip3 install meson

mkdir /home/$USER/src
cd /home/$USER/src

# install LiteX
wget https://raw.githubusercontent.com/enjoy-digital/litex/master/litex_setup.py
chmod +x litex_setup.py
./litex_setup.py --init --install --user

# RISCV toolchain
ARCHIVENAME="riscv64-unknown-elf-gcc-10.1.0-2020.08.2-x86_64-linux-ubuntu14"
RISCVURL="https://static.dev.sifive.com/dev-tools/freedom-tools/v2020.08/${ARCHIVENAME}.tar.gz"
wget "$RISCVURL"
tar -xvf "${ARCHIVENAME}.tar.gz"

echo "Linux-on-litex-vexriscv"
sudo apt install -y build-essential device-tree-compiler wget git python3-setuptools
git clone https://github.com/litex-hub/linux-on-litex-vexriscv
echo "Need to copy linux images over for the simulation, see this link for more info:"
echo "https://github.com/litex-hub/linux-on-litex-vexriscv/issues/164"
cd linux-on-litex-vexriscv
wget https://github.com/litex-hub/linux-on-litex-vexriscv/files/8331338/linux_2022_03_23.zip
unzip -d images/ linux_2022_03_23.zip
#wget https://github.com/litex-hub/linux-on-litex-vexriscv/files/8331418/arty_2022_03_23.zip
#wget https://github.com/litex-hub/linux-on-litex-vexriscv/files/8331388/orangecrab_2022_03_23.zip

# Verilator - 4.228
sudo apt install -y python3 make autoconf g++ flex bison ccache \
                    numactl libgoogle-perftools-dev perl-doc \
                    zlibc zlib1g-dev git

cd /home/$USER/src
git clone https://github.com/verilator/verilator
cd verilator
git checkout v4.228
autoconf
./configure --prefix=/usr/local/verilator
make -j$(nproc)
sudo make install

# Installing OpenOCD (only needed for hardware test)
cd /home/$USER/src
sudo apt install libtool automake pkg-config libusb-1.0-0-dev
git clone https://github.com/ntfreak/openocd.git
cd openocd
./bootstrap
./configure --enable-ftdi
make
sudo make install

# Fix the picolib version, just in case
cd /home/$USER/src
cd pythondata-software-picolibc/pythondata_software_picolibc/data
git checkout tags/1.7.9

# Ninja installation
sudo apt install ninja-build
#git clone https://github.com/ninja-build/ninja.git
#cd ninja
#git checkout tags/v1.11.1
#python3 configure.py --bootstrap

cd /home/$USER/src
echo "Script finished."

# setting up FPGA toolchain
# Inside chroot, sudo bash
# cd /home/$SUDO_USER/dev-env-setup
# ./hdl-tools-yosys
# If Xilinx Arty A7
# ./nextpnr-xilinx-install
# Or Lattice ECP5, TODO: Have not tested in a while, may be broken
# ./nextpnr-ecp5-install

# To produce vexriscv bitstream for arty a7
# cd /home/$USER/src/litex
# python3 -m litex_boards.targets.digilent_arty --build --load --toolchain=yosys+nextpnr

# Add convenience variable to chroot user .bash_profile
vars_for_bash_profile="export PATH=$PATH:/home/$USER/.local/bin:/home/$USER/src/$ARCHIVENAME/bin:/usr/local/verilator/bin/:/usr/local/nextpnr-xilinx/bin:/usr/local/nextpnr-xilinx"

echo -e "$vars_for_bash_profile" > /home/$USER/.bash_profile

echo "Setting /home/$USER/.bash_profile file to:
$vars_for_bash_profile"

echo "To run the vexriscv simulation, run the following:"
echo "cd /home/$USER/src/linux-on-litex-vexriscv"
echo "./sim.py"
